
package com.paulsdevblog.hellospringactuator.domain.service;

import com.paulsdevblog.hellospringactuator.domain.model.MyTime;

/**
 * Simple service interface to MyTime
 *
 * @author paulsdevblog.com
 */
public interface MyTimeService {

    MyTime getMyTime();
    
}
