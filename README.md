# SpringBoot-ActuatorMonitoring

This repo contains the SpringBoot project discussed within the respective PaulsDevBlog.com article. 
https://www.paulsdevblog.com/spring-boot-actuator-monitoring-your-app/

For other articles and code, please visit:
https://www.paulsdevblog.com