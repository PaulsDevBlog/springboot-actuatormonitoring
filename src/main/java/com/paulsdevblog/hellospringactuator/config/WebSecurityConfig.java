
package com.paulsdevblog.hellospringactuator.config;


import org.springframework.core.annotation.Order;
import org.springframework.context.annotation.Configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.autoconfigure.ManagementServerProperties;
import org.springframework.core.Ordered;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;

/**
 * Simple WebSecurityConfigurerAdapter to enable Spring Actuator
 * 
 * @author PaulsDevBlog.com
 */
@Configuration
@EnableWebSecurity
@Order( Ordered.HIGHEST_PRECEDENCE )
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser("monitoruser").password("monitorpwd").roles("ACTUATOR");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        
        http
            .httpBasic()
            .and()
            .csrf().disable()
            .authorizeRequests() //require detault HTTP authorization for every endpoint here
                .antMatchers("/monitoring/auditevents").permitAll()
                .antMatchers("/monitoring/env").permitAll()
                .antMatchers("/monitoring/health").permitAll()
                .antMatchers("/monitoring/info").permitAll()
                .antMatchers("/monitoring/logging").permitAll()
                .antMatchers("/monitoring/mappings").permitAll()
                .antMatchers("/monitoring/trace").permitAll()
                
        ;
    }

    
}
