
package com.paulsdevblog.hellospringactuator.domain.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * Simple entity example
 * This will hold date-time properties and the getters/setters used by the 
 * REST controller and jackson json libraries to render JSON views.
 *
 * @author www.paulsdevblog.com
 */
public class MyTime implements Serializable {

    @JsonIgnore
    private static final long serialVersionUID = 1L;
    
    private Date currentDate;
    private String currentIso8601Dt;
    private long  currentEpochSecond;

    public MyTime(){}
    
    public MyTime(
        Date currentDate,
        String currentIso8601Dt, 
        long currentEpochSecond
    ){
        this.currentDate = currentDate;
        this.currentIso8601Dt = currentIso8601Dt;
        this.currentEpochSecond = currentEpochSecond;
    }

    public Date getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(Date currentDate) {
        this.currentDate = currentDate;
    }

    public String getCurrentIso8601Dt() {
        return currentIso8601Dt;
    }

    public void setCurrentIso8601Dt(String currentIso8601Dt) {
        this.currentIso8601Dt = currentIso8601Dt;
    }

    public long getCurrentEpochSecond() {
        return currentEpochSecond;
    }

    public void setCurrentEpochSecond(long currentEpochSecond) {
        this.currentEpochSecond = currentEpochSecond;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.currentDate);
        hash = 79 * hash + Objects.hashCode(this.currentIso8601Dt);
        hash = 79 * hash + (int) (this.currentEpochSecond ^ (this.currentEpochSecond >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MyTime other = (MyTime) obj;
        if (this.currentEpochSecond != other.currentEpochSecond) {
            return false;
        }
        if (!Objects.equals(this.currentIso8601Dt, other.currentIso8601Dt)) {
            return false;
        }
        if (!Objects.equals(this.currentDate, other.currentDate)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "MyTime{" 
                    + "currentDate=" + currentDate 
                    + ", currentIso8601Dt=" + currentIso8601Dt 
                    + ", currentEpochSecond=" + currentEpochSecond 
                + '}';
    }
    
    
    
}
