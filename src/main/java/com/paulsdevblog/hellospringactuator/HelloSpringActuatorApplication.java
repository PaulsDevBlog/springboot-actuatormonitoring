package com.paulsdevblog.hellospringactuator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SpringBootApplication
public class HelloSpringActuatorApplication {
    
    public static final Logger logger = LoggerFactory.getLogger(HelloSpringActuatorApplication.class );

    public static void main(String[] args) {
        
        SpringApplication.run(HelloSpringActuatorApplication.class, args);
        
        logger.info("--Application Started--");
        logger.info("This is our Hello NetBeans and Hello World Micro-Service!");
    }
}
